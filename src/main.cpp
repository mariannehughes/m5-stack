#include <SPI.h> //include the SPI bus library
#include <MFRC522.h> // NFC Card Reader lib
#include <HTTPClient.h>
#include <WebServer.h>
#include "WifiManager.h"
#include <M5StackMod.h>
#include "Free_Fonts.h"

// ============== RC522 ============
#define RST_PIN         26          // Configurable, see typical pin layout above
#define SS_PIN          5         // Configurable, see typical pin layout above

// ================ App status ============
#define STT_LOGIN           1
#define STT_CONNECTION_ERR  2
#define STT_LOGIN_SUCCESS   3
#define STT_LOGIN_FAIL      4
#define STT_BTT_LOW         5
#define STT_WORKING         6
#define STT_LOGOUT_DISPLAY  7
#define STT_LOGEDOUT        8
#define STT_WIFI_ERR  		9
#define STT_LOGOUT_WAITING  10
#define STT_SCREENSAVER		11
#define STT_SETTING_SERVER	12

#define ERR_CONNECTION		1
#define ERR_BATTERY			2
#define ERR_LOGIN			3
#define ERR_WIFI			4
#define ERR_COMMON			5
#define ERR_VALUES			10
#define SCC_LOGIN			11
#define SCC_LOGOUT			12

#define HEIGHT_HEADER		30


#define TIME_SCREEN_SAVER	60000

// NFC card read
MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class
MFRC522::MIFARE_Key key; 

byte nuidPICC[6];
byte buffer[18];
byte size = 18;
int userId = 0;
int bundleId = 0;
int orderId = 0;
	
// Wifi
WiFiManager wifiManager;

// server addr
Preferences preference;
String 	strGateway = "";

unsigned long lastcheck = 0;
unsigned long scr_saver = 0;
int SignalStrength = 0;
int cntProduct = 0;
int cntDisplay = 100;
int appStatus = STT_LOGIN;
int prevAppStatus = STT_LOGIN;
// const char* ssid     = "3704";
// const char* password = "123456789";
bool fWorkingRedraw = true;

void drawHeader(){
	M5m.Lcd.setTextSize(1);
	M5m.WiFi_Mode = WiFi.getMode();
	M5m.Lcd.fillRect(0, 0, 320, HEIGHT_HEADER, BLUE);
	if (WiFi.isConnected())
	{
		M5m.Lcd.setTextColor(WHITE, BLUE);
		SignalStrength = map(100 + WiFi.RSSI(), 5, 90, 0, 100);
		M5m.Lcd.drawString("WiFi: " + String(SignalStrength) + "%", 10, 5, 2);
	}
	else
	{
		M5m.Lcd.setTextColor(WHITE, BLUE);
		M5m.Lcd.drawString("Wifi OFF", 10, 5, 2);
		// M5m.Lcd.drawString(WiFi.localIP(),150,10,1);
	}
}

void updateScreen (int stt){
	if(stt == STT_LOGIN){
		M5m.Lcd.fillRect(0,HEIGHT_HEADER,320,210,BLACK);
		M5m.Lcd.setTextColor(WHITE, BLACK);
		M5m.Lcd.setTextDatum(TC_DATUM); // Centre text on x,y position
		M5m.Lcd.setTextSize(2);
		M5m.Lcd.drawCentreString("Hello", M5m.Lcd.width() / 2, 40, 4);
		M5m.Lcd.drawCentreString("Please tap", M5m.Lcd.width() / 2, 90, 4);
		M5m.Lcd.drawCentreString("your ID", M5m.Lcd.width() / 2, 140, 4);
		// setting button
		M5m.Lcd.fillRoundRect(210,200,100,30,7,BLUE);
		M5m.Lcd.setTextSize(2);
		M5m.Lcd.setTextColor(WHITE, BLUE);
		M5m.Lcd.drawCentreString("Setting", 260, 206, 1);

		// // gateway addr
		M5m.Lcd.setTextSize(1);
		preference.begin("Gateway", false);
		strGateway = preference.getString("addr");
		M5m.Lcd.setTextColor(WHITE, BLACK);
		if (strGateway == "")
			M5m.Lcd.drawCentreString("No Gateway setting", 100, 206, 1);
		else
			M5m.Lcd.drawCentreString(strGateway, 100, 206, 1);
		preference.end();
	}
	else if (stt == STT_SETTING_SERVER){
		M5m.Lcd.fillRect(0,HEIGHT_HEADER,320,210,BLACK);
		M5m.Lcd.setTextColor(WHITE, BLACK);
		M5m.Lcd.setTextDatum(TC_DATUM); // Centre text on x,y position
		M5m.Lcd.setTextSize(2);
		M5m.Lcd.drawCentreString("Please tap", M5m.Lcd.width() / 2, 50, 2);
		M5m.Lcd.drawCentreString("Gateway Card", M5m.Lcd.width() / 2, 100, 2);
		// esc button
		M5m.Lcd.fillRoundRect(30,200,80,30,7,BLUE);
		M5m.Lcd.setTextSize(2);
		M5m.Lcd.setTextColor(WHITE, BLUE);
		M5m.Lcd.drawCentreString("Esc", 70, 206, 1);
	}
	else if (stt == STT_WORKING){
		if(cntDisplay != cntProduct || fWorkingRedraw){
			if(fWorkingRedraw){
				M5m.Lcd.fillRect(0,HEIGHT_HEADER,320,210,BLACK);
				M5m.Lcd.setTextSize(1);
				M5m.Lcd.setTextColor(WHITE, BLACK);
				M5m.Lcd.drawCentreString("Tap what you make", M5m.Lcd.width() / 2, 75, 4);
				M5m.Lcd.setTextDatum(TC_DATUM);
				M5m.Lcd.setTextSize(4);
				M5m.Lcd.drawNumber(cntProduct, M5m.Lcd.width() / 2, 110, 2);
				M5m.Lcd.fillRoundRect(120,200,80,30,7,PINK);
				M5m.Lcd.setTextSize(2);
				M5m.Lcd.setTextColor(WHITE, PINK);
				M5m.Lcd.drawCentreString("Stop", M5m.Lcd.width() / 2, 206, 1);
				fWorkingRedraw = false;
			}
			else{
				M5m.Lcd.setTextColor(WHITE, BLACK);
				M5m.Lcd.setTextDatum(TC_DATUM);
				M5m.Lcd.setTextSize(4);
				M5m.Lcd.drawNumber(cntProduct, M5m.Lcd.width() / 2, 110, 2);
			}
			cntDisplay = cntProduct;
		}
	}
	else if (stt == STT_LOGOUT_DISPLAY){
		M5m.Lcd.fillRect(0,HEIGHT_HEADER,320,210,BLACK);
		M5m.Lcd.setTextSize(2);
		M5m.Lcd.setTextColor(RED, BLACK);
		M5m.Lcd.drawCentreString("Stop work", M5m.Lcd.width() / 2, 75, 2);
		M5m.Lcd.setTextColor(WHITE, BLACK);
		M5m.Lcd.drawCentreString("Are you sure?", M5m.Lcd.width() / 2, 125, 2);

		M5m.Lcd.fillRoundRect(40,200,80,30,7,PINK);
		M5m.Lcd.setTextSize(2);
		M5m.Lcd.setTextColor(WHITE, PINK);
		M5m.Lcd.drawCentreString("No", 80, 206, 1);

		M5m.Lcd.fillRoundRect(200,200,80,30,7,PINK);
		M5m.Lcd.setTextSize(2);
		M5m.Lcd.setTextColor(WHITE, PINK);
		M5m.Lcd.drawCentreString("Yes", 240, 206, 1);
	}
}

void drawTempScreen(int type){
	M5m.Lcd.fillRect(0,HEIGHT_HEADER,320,210,BLACK);
	if(type <= ERR_VALUES){
		// error screens
		M5m.Lcd.fillTriangle(110,62,190,143,207,127,RED);
		M5m.Lcd.fillTriangle(207,127,127,46,110,62,RED);
		M5m.Lcd.fillTriangle(110,127,127,143,207,62,RED);
		M5m.Lcd.fillTriangle(207,62,190,46,110,127,RED);
		M5m.Lcd.setTextDatum(TC_DATUM); // Centre text on x,y position
		M5m.Lcd.setTextSize(2);
		M5m.Lcd.setTextColor(WHITE, BLACK);
		if(type == ERR_WIFI){
			M5m.Lcd.drawCentreString("No Wi-Fi", M5m.Lcd.width() / 2, 155, 2);
			M5m.Lcd.drawCentreString("Connection", M5m.Lcd.width() / 2, 190, 2);
		}
		else if (type == ERR_BATTERY){
			M5m.Lcd.drawCentreString("Low", M5m.Lcd.width() / 2, 155, 2);
			M5m.Lcd.drawCentreString("Battery", M5m.Lcd.width() / 2, 190, 2);
		}
		else if (type == ERR_CONNECTION){
			M5m.Lcd.drawCentreString("Connection", M5m.Lcd.width() / 2, 155, 2);
			M5m.Lcd.drawCentreString("Problem", M5m.Lcd.width() / 2, 190, 2);
		}
		else if (type == ERR_LOGIN){
			M5m.Lcd.drawCentreString("ID not", M5m.Lcd.width() / 2, 155, 2);
			M5m.Lcd.drawCentreString("recognized", M5m.Lcd.width() / 2, 190, 2);
		}
		else if (type == ERR_COMMON){
			M5m.Lcd.drawCentreString("Something went", M5m.Lcd.width() / 2, 155, 2);
			M5m.Lcd.drawCentreString("wrong", M5m.Lcd.width() / 2, 190, 2);
		}
	}
	else{
		if (type == SCC_LOGIN){
			M5m.Lcd.fillTriangle(222,64,134,194,243,99,GREEN);
			M5m.Lcd.fillTriangle(222,64,135,166,134,194,GREEN);
			M5m.Lcd.fillTriangle(92,130,134,194,135,166,GREEN);
			M5m.Lcd.fillTriangle(92,130,73,146,134,194,GREEN);
		}
		else{
			M5m.Lcd.fillTriangle(109,98,96,112,140,142,GREEN);
			M5m.Lcd.fillTriangle(109,98,140,142,140,122,GREEN);
			M5m.Lcd.fillTriangle(140,122,140,142,216,77,GREEN);
			M5m.Lcd.fillTriangle(140,122,216,77,200,53,GREEN);
			M5m.Lcd.setTextDatum(TC_DATUM); // Centre text on x,y position
			M5m.Lcd.setTextSize(2);
			M5m.Lcd.setTextColor(WHITE, BLACK);
			M5m.Lcd.drawCentreString("You have been", M5m.Lcd.width() / 2, 155, 2);
			M5m.Lcd.drawCentreString("logged out", M5m.Lcd.width() / 2, 190, 2);
		}
	}
}

void beepSound(){
	M5m.Speaker.beep();
	delay(60);
	M5m.Speaker.update();
}

bool checkNewNFCCard(){
	if (!rfid.PICC_IsNewCardPresent())
		return false;
	// Verify if the NUID has been readed
	if (!rfid.PICC_ReadCardSerial())
		return false;

	// MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
	if (rfid.uid.uidByte[0] != nuidPICC[0] || 
		rfid.uid.uidByte[1] != nuidPICC[1] || 
		rfid.uid.uidByte[2] != nuidPICC[2] || 
		rfid.uid.uidByte[3] != nuidPICC[3] || 
		rfid.uid.uidByte[4] != nuidPICC[4] || 
		rfid.uid.uidByte[5] != nuidPICC[5] ){
			// Store NUID into nuidPICC array
			for (byte i = 0; i < rfid.uid.size; i++) {
				nuidPICC[i] = rfid.uid.uidByte[i];
			}
			return true;
		}
	return false;
}

bool readBlock(int pageNumber) {
	// // int largestModulo4Number=pageNumber/4*4;
	// int largestModulo4Number=4;
	// int trailerBlock=largestModulo4Number+3;//determine trailer block for the sector
	// MFRC522::StatusCode auth_code = (MFRC522::StatusCode) rfid.PCD_Authenticate(0x60, trailerBlock, &key, &(rfid.uid));

	// if (auth_code != MFRC522::STATUS_OK) {
	// 	M5m.Lcd.print("Auth failed  " + String(auth_code));
	// 	delay(1000);
	// 	return false;
	// }
	
	byte status = rfid.MIFARE_Read(pageNumber, buffer, &size);
	rfid.PICC_HaltA();

	if (status != MFRC522::STATUS_OK) {
		M5m.Lcd.print("Read failed: ");
		return false;
	}
	
	// rfid.PCD_StopCrypto1();
	return true;
}

void printBuffer(){
	for(int i=0; i<4; i++){
		M5m.Lcd.printf("page %d:  %02X %02X %02X %02X \r\n",i+6, buffer[i*4+3], buffer[i*4+2], buffer[i*4+1], buffer[i*4+0]);
	}
	delay(3000);
}

int getIntValue(int pageNumber){
	pageNumber -= 6;
	uint a = buffer[pageNumber * 4 + 3];
	uint b = buffer[pageNumber * 4 + 2];
	uint c = buffer[pageNumber * 4 + 1];
	uint d = buffer[pageNumber * 4 + 0];
	int temp = a * 24 + b * 16 + c * 8 + d;
	return temp;
}

String getGateWayAddress(){
	String str = "";
	int a = getIntValue(6);
	int b = getIntValue(7);
	int c = getIntValue(8);
	int d = getIntValue(9);
	str = "http://" + String(a) + "." + String(b) + "." + String(c) + "." + String(d) + ":8080";
	return str;
}

// ================================ Setup =================================
void setup(){
	M5m.begin();
	SPI.begin(); // Init SPI bus
    rfid.PCD_Init(); // Init MFRC522 

	// Since the cards in the kit are new and the keys were never defined, they are 0xFF
	for (byte i = 0; i < 6; i++) {
		key.keyByte[i] = 0xFF; //keyByte is defined in the "MIFARE_Key" 'struct' definition in the .h file of the library
	} 
	
	M5m.Speaker.setBeep(300,30);

	// read gateway address
	preference.begin("Gateway", false);
	strGateway = preference.getString("addr");
	
	if (strGateway == "")
    	appStatus = STT_SETTING_SERVER;
	else
		appStatus = STT_LOGIN;
	preference.end();
    // wifiManager.resetSettings();

	M5m.Lcd.setTextSize(2);
	M5m.Lcd.setTextColor(WHITE, BLACK);
	M5m.Lcd.drawCentreString("This screen disappears", M5m.Lcd.width() / 2, 55, 2);
	M5m.Lcd.drawCentreString("after 1 second.", M5m.Lcd.width() / 2, 85, 2);
	M5m.Lcd.drawCentreString("If not, please set up", M5m.Lcd.width() / 2, 125, 2);
	M5m.Lcd.drawCentreString("your wifi.", M5m.Lcd.width() / 2, 155, 2);

    wifiManager.autoConnect("AutoConnectAP");
	updateScreen(appStatus);
	scr_saver = millis();
}

// ================================ Loop =================================
void loop(){
	unsigned long now = millis();
	if (now - scr_saver >= TIME_SCREEN_SAVER){
		prevAppStatus = appStatus;
		appStatus = STT_SCREENSAVER;
		M5m.Lcd.clear();
		M5m.Lcd.setTextSize(2);
		M5m.Lcd.setTextColor(WHITE,BLACK);
		M5m.Lcd.drawString("Press any Button", M5m.Lcd.width() / 2, 120,2);
	}
	if (appStatus == STT_SCREENSAVER){
		scr_saver = millis();
		M5m.update();
		if (M5m.BtnB.wasPressed() || M5m.BtnA.wasPressed() || M5m.BtnC.wasPressed() ){
			appStatus = prevAppStatus;
			beepSound();
			fWorkingRedraw = true;
			if (prevAppStatus == STT_LOGOUT_WAITING)
				updateScreen(STT_LOGOUT_DISPLAY);
			else
				updateScreen(appStatus);
		}
		return;
	}

	if (now - lastcheck >= 1000){
		drawHeader();
		lastcheck = now;
		for (int i = 0; i < 6; i++) {
			nuidPICC[i] = 0x00;
		}
	}
	if (!WiFi.isConnected()){
		drawTempScreen(ERR_WIFI);
		// setup_wifi();
		appStatus = STT_WIFI_ERR;
		delay(500);
		return;
	}

	// M5m.update();
	// M5m.Speaker.update();
	if (appStatus == STT_WIFI_ERR){
		delay(100);
		appStatus = STT_LOGIN;	
		updateScreen(appStatus);
		return;
	}
	else if (appStatus == STT_SETTING_SERVER){
		M5m.update();
		if (M5m.BtnA.wasPressed()){
			appStatus = STT_LOGIN;
			beepSound();
			updateScreen(appStatus);
			return;
		}
 		if (checkNewNFCCard()) {
			scr_saver = millis();
			// M5m.Lcd.print(F("In hex: "));
			if (readBlock(6)){
				beepSound();
				printBuffer();
				// preference put
				String temp = getGateWayAddress();
				preference.begin("Gateway", false);
				preference.putString("addr", temp);
				preference.end();
				drawTempScreen(SCC_LOGIN);
				delay(1000);
				appStatus = STT_LOGIN;
				updateScreen(appStatus);
			}
		}
	}
	else if (appStatus == STT_LOGIN){
		M5m.update();
		if (M5m.BtnC.wasPressed()){
			appStatus = STT_SETTING_SERVER;
			beepSound();
			updateScreen(appStatus);
			return;
		}

		if (checkNewNFCCard()) {
			scr_saver = millis();

			if (readBlock(6)){
				// printBuffer();
				beepSound();
				userId = getIntValue(6);
				if (userId == 0){
					M5m.Lcd.println("Page 6 - Worker ID is 0");
					appStatus = STT_LOGIN_FAIL;
					return;
				}	
				else{
					appStatus = STT_LOGIN_SUCCESS;
					HTTPClient http;
					String url = strGateway + "/workers" + String(userId) + "/session";
					http.begin(url);  //Specify destination for HTTP request
					int httpResponseCode = http.POST("");//("POSTING from ESP32");   //Send the actual POST request
					M5m.Lcd.println(httpResponseCode);
					if(httpResponseCode == 200){
						String response = http.getString();
						char code = response.charAt(0);
						if (code==0x01){
							appStatus = STT_LOGIN_SUCCESS;
						}
						else if (code == 0x00){
							appStatus = STT_LOGIN_FAIL;
						}
						else {
							appStatus = STT_CONNECTION_ERR;
						}
					}
					else{
						M5m.Lcd.print("Error on sending POST: ");
						M5m.Lcd.println(httpResponseCode);
						appStatus = STT_CONNECTION_ERR;
					}
					delay(500);
					http.end();  //Free resources
				}
			}
		}
	}
	else if (appStatus == STT_LOGIN_SUCCESS){
		drawTempScreen(SCC_LOGIN);
		delay(1000);
		appStatus = STT_WORKING;
		updateScreen(appStatus);
	}
	else if (appStatus == STT_LOGIN_FAIL){
		drawTempScreen(ERR_LOGIN);
		delay(1000);
		appStatus = STT_LOGIN;	
		updateScreen(appStatus);
	}
	else if (appStatus == STT_CONNECTION_ERR){
		drawTempScreen(ERR_CONNECTION);
		delay(1000);
		appStatus = STT_LOGIN;	
		updateScreen(appStatus);
	}
	else if (appStatus == STT_WORKING){
		updateScreen(appStatus);
		M5m.update();
		if (M5m.BtnB.wasPressed()){
			scr_saver = millis();
			appStatus = STT_LOGOUT_DISPLAY;
			M5m.Speaker.beep();
			delay(60);
			M5m.Speaker.update();
			return;
		}

		if (checkNewNFCCard()) {
			scr_saver = millis();

			if (readBlock(6)){
				// printBuffer();
				beepSound();
				bundleId = getIntValue(6);
				orderId = getIntValue(7);
				if (bundleId == 0){
					M5m.Lcd.println("Page 6 - Bundle ID is 0");
					return;
				}	
				else{
					HTTPClient http;
					String url = strGateway + "/workers" + String(userId) + "/bundle";
					
					String data = '{"bundle_id:"' + String(bundleId) + ',"product_id":' + String(orderId) + '}';
					http.begin(url);  //Specify destination for HTTP request
					int httpResponseCode = http.POST(data);//("POSTING from ESP32");   //Send the actual POST request
					M5m.Lcd.println(httpResponseCode);
					if(httpResponseCode == 200){
						String response = http.getString();
						char code = response.charAt(0);
						if (code==0x01){
							cntProduct ++;
						}
						else if (code == 0x00){
							drawTempScreen(ERR_COMMON);
							delay(1000);
							appStatus = STT_WORKING;
							fWorkingRedraw = true;	
							updateScreen(appStatus);
						}
						else {
							appStatus = STT_CONNECTION_ERR;
						}
					}
					else{
						M5m.Lcd.print("Error on sending POST: ");
						M5m.Lcd.println(httpResponseCode);
						appStatus = STT_CONNECTION_ERR;
					}
					delay(500);
					http.end();  //Free resources
				}
			}
		}
	}
	else if (appStatus == STT_LOGOUT_DISPLAY){
		updateScreen(appStatus);
		delay(1000);
		appStatus = STT_LOGOUT_WAITING;
	}
	else if (appStatus == STT_LOGOUT_WAITING){
		fWorkingRedraw = true;
		// scr_saver = millis();
		M5m.update();
		if (M5m.BtnA.wasPressed()){
			appStatus = STT_WORKING;
			M5m.Speaker.beep();
			delay(60);
			M5m.Speaker.update();
			return;
		}
		if (M5m.BtnC.wasPressed()){
			HTTPClient http;
			String url = strGateway + "/workers" + String(userId) + "/session/close";
			http.begin(url);  //Specify destination for HTTP request
			int httpResponseCode = http.POST("");//("POSTING from ESP32");   //Send the actual POST request
			M5m.Lcd.println(httpResponseCode);
			if(httpResponseCode == 200){
				String response = http.getString();
				char code = response.charAt(0);
				if (code==0x01){
					appStatus = STT_LOGEDOUT;
					beepSound();
					return;
				}
				else if (code == 0x00){
					drawTempScreen(ERR_COMMON);
					delay(1000);
					appStatus = STT_LOGOUT_WAITING;
					updateScreen(appStatus);
				}
				else {
					appStatus = STT_CONNECTION_ERR;
				}
			}
			else{
				M5m.Lcd.print("Error on sending POST: ");
				M5m.Lcd.println(httpResponseCode);
				appStatus = STT_CONNECTION_ERR;
			}
			delay(500);
			http.end();  //Free resources
		}
	}
	else if (appStatus == STT_LOGEDOUT){
		drawTempScreen(SCC_LOGOUT);
		delay(1000);
		appStatus = STT_LOGIN;
		cntProduct = 0;
		cntDisplay = 100;
		updateScreen(appStatus);
	}

}
